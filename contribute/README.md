# How to contribute?

Beispieldatei der Westnetz zeitfenster-fuer-atypische-netznutzung-2020.pdf gibt  
einen Überblick der Spannungsebenen sowie deren Zeiträume. Feiertage, Brückentage  
sowie die Zeit zwischen Weihnachten und Neujahr müssen nicht in den CSV Dateien  
berücksichtigt werden.

Definition der Netz- und Umspannebene:
HOES = Höchstspannung mit Umspannung auf Hochspannung
HS = Hochspannung
HS/MS = Hochspannung mit Umspannung auf Mittelspannung
MS = Mittelspannung
MS/NS = Mittelspannung mit Umspannung auf Niederspannung
NS = Niederspannung

bdew = BDEWCodenummer for the dno https://bdew-codes.de/Codenumbers/BDEWCodes/CodeOverview
dno = distribution network operator
vs = voltage level (HOES,HS,HS/MS,MS,MS/NS,NS)
sd = start date of frame (dd.mm.yyyy) example: 01.01.2020
ed = end date of frame (dd.mm.yyyy) example: 29.02.2021
st = start time (HH:MM) example: 07:00
et = end time (HH:MM) example: 09:00

CSV Header:
bdew,dno,vl,sd,ed,st,et

from the Westnetz example above:

Westnetz GmbH
BDEWCodenummer: 664751

bdew,dno,vl,sd,ed,st,et
664751,Westnetz GmbH,HS,01.01.2020,29.02.2021,09:45,19:45
664751,Westnetz GmbH,HS/MS,01.01.2020,29.02.2021,09:45,19:45
664751,Westnetz GmbH,MS,01.09.2020,29.02.2021,09:30,19:30
664751,Westnetz GmbH,MS/NS,01.09.2020,30.11.2020,17:00,19:15
664751,Westnetz GmbH,MS/NS,01.01.2020,29.02.2021,16:30,20:00
664751,Westnetz GmbH,NS,01.09.2020,30.11.2021,17:15,19:15
664751,Westnetz GmbH,NS,01.01.2020,29.02.2021,12:15,12:45
664751,Westnetz GmbH,NS,01.01.2020,29.02.2021,16:30,20:15

resulting Excel/Spreadsheet Table:

![Spreadsheet Example](./example.png)
