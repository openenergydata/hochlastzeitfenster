import csv, pytz, sys, logging, holidays
from datetime import datetime, timedelta

TZBERLIN = pytz.timezone('Europe/Amsterdam')

# input, date and time are in Timezone Europe/Berlin (GMT+1)
# bdew = BDEWCodenummer for the dno https://bdew-codes.de/Codenumbers/BDEWCodes/CodeOverview
# dno = distribution network operator
# vs = voltage level (HOES,HS,HS/MS,MS,MS/NS,NS)
# sd = start date of frame (dd.mm.yyyy) example: 01.01.2020, 
# ed = end date of frame (dd.mm.yyyy) example: 29.02.2021, 
# st = start time (HH:MM) example: 07:00, 07:00  - 07:15, start of time interval
# et = end time (HH:MM) example: 09:00, -> 09:00 - 09:15, start of time interval
# CSV Header: bdew,dno,vl,sd,ed,st,et

#bdew,dno,vl,sd,ed,st,et
#664751,Westnetz GmbH,HS,01.01.2020,29.02.2021,09:45,19:45
#664751,Westnetz GmbH,HS/MS,01.01.2020,29.02.2021,09:45,19:45
#664751,Westnetz GmbH,MS,01.09.2020,29.02.2021,09:30,19:30
#664751,Westnetz GmbH,MS/NS,01.09.2020,30.11.2020,17:00,19:15
#664751,Westnetz GmbH,MS/NS,01.01.2020,29.02.2021,16:30,20:00
#664751,Westnetz GmbH,NS,01.09.2020,30.11.2021,17:15,19:15
#664751,Westnetz GmbH,NS,01.01.2020,29.02.2021,12:15,12:45
#664751,Westnetz GmbH,NS,01.01.2020,29.02.2021,16:30,20:15

hlzfDict = {}
# 2021
hlzfDict[2021] = []
hlzfDict[2021].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.01.2021','ed':'28.02.2021','st':'16:45','et':'19:30'})
hlzfDict[2021].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.12.2021','ed':'31.12.2021','st':'16:45','et':'19:30'})
# 2020
hlzfDict[2020] = []
hlzfDict[2020].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.01.2020','ed':'28.02.2020','st':'16:45','et':'19:30'})
hlzfDict[2020].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.12.2020','ed':'31.12.2020','st':'16:45','et':'19:30'})
# 2019
hlzfDict[2019] = []
hlzfDict[2019].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.01.2019','ed':'28.02.2019','st':'16:45','et':'19:30'})
hlzfDict[2019].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.12.2019','ed':'31.12.2019','st':'16:45','et':'19:30'})
# 2018
hlzfDict[2018] = []
hlzfDict[2018].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.01.2018','ed':'28.02.2018','st':'16:30','et':'19:30'})
hlzfDict[2018].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.09.2018','ed':'30.11.2018','st':'16:45','et':'19:30'})
hlzfDict[2018].append({'bdew':815,'dno':'AllgäuNetz GmbH & Co. KG','prov':'BY','vl':'MS','sd':'01.12.2018','ed':'31.12.2018','st':'16:30','et':'19:30'})

def checkHLZF(dt=None, voltagelevel=None, quarterTimeDrift=0):
    hlzList = []
    germanHolidays = {}
    germanHolidays['default'] = holidays.Germany(years=dt.year)

    # no dt no hlz
    if dt is None:
        return False

    # todo validate Month...

    # Die angegebenen Hochlastzeitfenster liegen ausschließlich an Werktagen (Montag – Freitag) vor. 
    # Wochenenden, Feiertage, max. ein Brückentag sowie die Zeit zwischen Weihnachten und Neujahr gelten als Nebenzeiten. 
    # Bei den Zeiten ist jeweils der Beginn des entsprechenden ¼-Stunden-Intervalls angegeben.
    # Beispiel: 08:00-13:15 bedeutet [08:00; 13:30]

    # check time between christmas and new year
    christmas = TZBERLIN.localize(datetime(dt.year, 12, 24))
    newyear = TZBERLIN.localize(datetime(dt.year+1, 1, 1))
    if dt > christmas and dt < newyear:
        return False

    # check Weekday
    #0 Monday,1 Tuesday, 2 Wednesday, 3 Thursday, 4 Friday, 5 Saturday, 6 Sunday
    if dt.weekday() >= 5:
        return False

    if dt.year in hlzfDict:
        hlzList = hlzfDict[dt.year]


    # define german holiday for province
    for item in hlzList:
        if 'prov' in item:
            prov = item['prov']
            if prov in ['BW', 'BY', 'BYP', 'BE', 'BB', 'HB', 'HH', 'HE', 'MV', 'NI', 'NW', 'RP', 'SL', 'SN', 'ST', 'SH', 'TH']:
                germanHolidays[prov] = holidays.Germany(prov=prov, years=dt.year)
    
    # check german holiday (default)
    if len(germanHolidays) == 1:
        for holidayDict in germanHolidays:
            if dt in holidayDict:
                return False

    try:
        for item in hlzList:
            # compare volate level
            if voltagelevel is not None:
                if voltagelevel != item['vl']:
                    return False

            # compare province holidays
            if len(germanHolidays) > 1:
                if 'prov' in item:
                    if item['prov'] in germanHolidays:
                        if dt in germanHolidays[item['prov']]:
                            return False


            
            st = TZBERLIN.localize(datetime.strptime(item['st'], "%H:%M")) 
            et = TZBERLIN.localize(datetime.strptime(item['et'], "%H:%M")) 
            sd = TZBERLIN.localize(datetime.strptime(item['sd'], "%d.%m.%Y")) + timedelta(hours=st.hour, minutes=st.minute)
            ed = TZBERLIN.localize(datetime.strptime(item['ed'], "%d.%m.%Y")) + timedelta(hours=et.hour, minutes=et.minute)

            # check start day,hour,minute and end day,hour,minutes
            if dt >= sd and dt <= ed:
                # check quarterTimeDrift, only 600 minutes -/+ allowed
                # set a quarter timedrift, for example to drift start of quarter to end of quarter
                if quarterTimeDrift > 0 and quarterTimeDrift <= 600 and quarterTimeDrift % 15 == 0:
                    sd = TZBERLIN.localize(datetime(dt.year, dt.month, dt.day, st.hour, st.minute)) + timedelta(minutes=quarterTimeDrift)
                    ed = TZBERLIN.localize(datetime(dt.year, dt.month, dt.day, et.hour, et.minute)) + timedelta(minutes=quarterTimeDrift)
                elif quarterTimeDrift < 0 and quarterTimeDrift <= -600 and quarterTimeDrift % 15 == 0:
                    sd = TZBERLIN.localize(datetime(dt.year, dt.month, dt.day, st.hour, st.minute)) - timedelta(minutes=quarterTimeDrift)
                    ed = TZBERLIN.localize(datetime(dt.year, dt.month, dt.day, et.hour, et.minute)) - timedelta(minutes=quarterTimeDrift)
                else:
                    sd = TZBERLIN.localize(datetime(dt.year, dt.month, dt.day, st.hour, st.minute))
                    ed = TZBERLIN.localize(datetime(dt.year, dt.month, dt.day, et.hour, et.minute))

                # check hour,minutes of day
                if dt >= sd and dt <= ed:
                    return True

    except Exception as e:
        logging.error("Error checkHLZ: %s", str(e))
        return False

    return False        


def getHLZRow(dt, voltagelevel=None, TZ=TZBERLIN, quarterTimeDrift=0, fill=None, dummy=False):
    row = False

    try:
        keyDate = dt.strftime("%d.%m.%Y %H:%M")
    except Exception as e:
        logging.error("parse datetime for dt=%s, %s", str(dt), str(e))
        return row
    
    try:
        response = 1 if checkHLZF(dt=dt, voltagelevel=voltagelevel, quarterTimeDrift=quarterTimeDrift) else 0
        # generate dummy data
        response = 0 if dummy else response
    except Exception as e:
        logging.error("checkHLZ for dt=%s voltagelevel=%s %s", str(dt), str(voltagelevel), str(e))
        return row
    
    try:
        if response:
            # hlz active
            row = { 'datum':keyDate, 'aktiv':response, 'spannungsebene':voltagelevel }
        else:
            # hlz deactive
            if fill:
                row = { 'datum':keyDate, 'aktiv':response, 'spannungsebene':voltagelevel }
    except Exception as e:
        logging.error("set row[keyData] failed %s ", str(e))
    
    return row

# todo, start or end of timeinterval?
# todo, choose, dno and voltagelevel for checkHLZ

def main(year=datetime.now().year, fill=True, dummy=False, quarterTimeDrift=15):
    logging.basicConfig(encoding='utf-8', level=logging.INFO)

    csvHeader = ['datum', 'aktiv', 'spannungsebene']
    voltagelevel = "MS"
    csvContent = {}

    if year >= 1990 and year <= 2030:
        startDate = TZBERLIN.localize(datetime(year, 1, 1, 0, 0))
        endDate = TZBERLIN.localize(datetime(year+1, 1, 1, 0, 0))
    else:
        year = datetime.now().year
        startDate = TZBERLIN.localize(datetime(year, 1, 1, 0, 0))
        endDate = TZBERLIN.localize(datetime(year+1, 1, 1, 0, 0))

    dt = startDate

    logging.info("HLZF, startdate: %s, enddate: %s, voltagelevel: %s, fill: %s dummy: %s quarterTimeDrift: %s", str(startDate), str(endDate), str(voltagelevel), str(fill), str(dummy), str(quarterTimeDrift))


    while dt < endDate:
        response = getHLZRow(dt=dt, voltagelevel=voltagelevel, fill=fill, dummy=dummy, quarterTimeDrift=quarterTimeDrift)
        if response and len(response):
            
            csvContent[response['datum']] = response
        dt = dt + timedelta(minutes=15)

    dummyname = "_dummy" if dummy else ""
    fillname = "_fill" if fill else ""
    qtdname = "_qtd" + str(quarterTimeDrift) if quarterTimeDrift != 0 else ""
    year = "_" + str(startDate.year)
    voltagelevel = "_" + str(voltagelevel)
    filename = "HLZ_Allgaeunetz"
    filename = filename + voltagelevel + fillname + dummyname + qtdname + year + ".csv"

    logging.info("writing file...")

    with open(filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csvHeader)
        writer.writeheader()
        for key,row in csvContent.items():
            writer.writerow(row)
    
    logging.info("CSV File %s with %s rows are written", str(filename), str(len(csvContent)))

if __name__ == "__main__":
    year = 0
    if len(sys.argv) != 2:
        print('Invalid Numbers of Arguments. Script will be terminated.')
    else:
        year = int(sys.argv[1])
    main(year)


