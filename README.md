# Hochlastzeitfenster

Repository to share community collected peak load time windows from german distribution system operators (DSO).

### how it works?

The german DSOs publish annually their peak load time windows for their distribution network.  
DSOs are obliged to publish these peak-load time windows, but not in machine readable form.  
This results in different published formats and forms which cannot be read automatically.

The project aims at preparing all data of the 900 DSOs in Germany and making them available free of charge.

Further details on the topic of peak-load time windows: https://de.wikipedia.org/wiki/Atypische_Netznutzung

### how to use it?

In the repository there is a CSV file for each year which contains the peak load time windows  
of a voltage level of the collected DSOs.  
By entering the peak load time windows in the CSV file, the unstructured published data of the  
network operators are standardised and made machine-readable.

- clone the repository and use collected files from the community directly

### how to contribute?

Anyone can take part. The peak load time windows will be published on the website of the DSO.  
These are provided in PDF format, for example. The task is to enter the peak-load time windows from the PDF  
into the appropriate CSV file of the correct voltage level.

More information on how exactly the entry works and how you can participate can be found under contribute

### further ideas and projects:

- API to serve the data
- API to serve some calculation
- A map to show all DSO, with peak-load time windows, and prices
- Collect all DSOs from BNetzA or BDEWCodes, check data status and percentage of dno´s
- https://www.marktstammdatenregister.de/MaStR/Akteur/Marktakteur/IndexOeffentlich
- Collect some peak-load time windows data automated from dno website/pdf
- generate a website to crawl the dno with peak-load time windows (pages?)
